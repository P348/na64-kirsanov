  IPrint      = 1;
  Mode        = 0;
  ICalib      = 0;
  TimeCut     = 4.;

  ECALBadEnergyCut = 1.;
  StrawCut = 1.;
  MomentumLowerCut = -100.;
  MomentumUpperCut = 10000.;
  MomentumChiSqLowerCut = -1.;
  MomentumChiSqUpperCut = 40.;
  SRDLowerCut = 10.;
  SRDUpperCut = 120.;
  SRDEachLowerCut = 2.;
  WCAL0LowerCut = -1.;
  WCAL0UpperCut = 1000.;
  WCAL1LowerCut = -1.;
  WCAL1UpperCut = 1000.;
  WCAL2LowerCut = -1.;
  WCAL2UpperCut = 1000.;
  WCATVetoUpperCut = 1000.;
  WCALSumLowerCut = -1.;
  WCALSumUpperCut = 1000.;
  ECALLowerCut = -1.;
  ECALUpperCut = 80.;
  ECALLowerCut2 = -1.;
  PRSLowerCut = 0.4;
  PRSUpperCut = 1000.;
  WCALECALLowerCut = -1.;
  WCALECALUpperCut = 1000.;
  WCALECALLowerCut2 = -1.;
  ECALMaxCell = 30;
  ECALNarrowShower = -1.;
  ECALChi2UpperCut = 8.;
  
  V2LowerCut = -1.;
  V2UpperCut = 1000.;
  S4LowerCut = -1.;
  S4UpperCut = 1000.;

  VETOLowerCut = -1.;
  VETOUpperCut = 10000.;
  VETOEachLowerCut = -1.;
  VETOEachUpperCut = 0.007;
  VETOCentralLowerCut = -1.;
  VETOCentralUpperCut = 0.007;
  HCAL0LowerCut = -1.;
  HCAL0UpperCut = 1.5;
  HCAL1LowerCut = -1.;
  HCAL1UpperCut = 1000.;
  HCAL2LowerCut = -1.;
  HCAL2UpperCut = 1000.;
  HCAL3LowerCut = -1.;
  HCAL3UpperCut = 3.5;
  HCALSumLowerCut = 16.;
  HCALSumUpperCut = 1000.;
  Rhcal12UpperCut = 0.05; // signal box
  Rhcal12LowerCut = -1.;  // signal box
  ETotLowerCut = -1.;
