void timehisto(RecoEvent &e)
{
  const bool hasMaster = (e.masterTime != 0);

  if(!hasMaster) return;

  const double master = e.masterTime;

  //const double master = SADC_sampling_interval * e.T2.t0_CenterOfMass(12, 20);

  //const double master = SADC_sampling_interval * e.S2.t0_CenterOfMass(12, 20);

  //const double master = SADC_sampling_interval * e.T2.t0;

  //SRD histo
  int imod=1;
  //ts0->Fill(e.S1.t0ns());
  ts0->Fill(master);
  if(e.SRD[imod].hasDigit) {
    tsrd->Fill(e.SRD[imod].t0ns());
    //tdiffsrd->Fill(e.SRD[imod].t0ns() - master);
    //tdiffsrd->Fill(SADC_sampling_interval*e.SRD[imod].t0_CenterOfMass(10, 17) - master);
    tdiffsrd->Fill( (e.SRD[imod].t0ns() - master - SRD_calibration[imod].tmean)/SRD_calibration[imod].tsigma );
  }

  //ECAL histo
  int ipart=1;
  int ix=2;
  int iy=2;
  if(e.ECAL[ipart][iy][ix].hasDigit) {
    tecal->Fill(e.ECAL[ipart][iy][ix].t0ns());
    //tdiffecal->Fill(e.ECAL[ipart][iy][ix].t0ns() - master);
    tdiffecal->Fill( (e.ECAL[ipart][iy][ix].t0ns() - master - ECAL_calibration[ipart][iy][ix].tmean)/ECAL_calibration[ipart][iy][ix].tsigma );
  }

  //HCAL histo
  imod=0;
  ix=2;
  iy=0;
  if(e.HCAL[imod][ix][iy].hasDigit && e.HCAL[imod][ix][iy].energy > 1.5) {
    thcal->Fill(e.HCAL[imod][ix][iy].t0ns());
    //tdiffhcal->Fill(e.HCAL[imod][ix][iy].t0ns() - master);
    tdiffhcal->Fill( (e.HCAL[imod][ix][iy].t0ns() - master - HCAL_calibration[imod][ix][iy].tmean)/HCAL_calibration[imod][ix][iy].tsigma );
  }

  //WCAL histo
  int ipm=2;
  if(e.WCAL[ipm].hasDigit) {
    tdet->Fill(e.WCAL[ipm].t0ns());
    //tdiffdet->Fill(e.WCAL[ipm].t0ns() - master);
    tdiffdet->Fill( (e.WCAL[ipm].t0ns() - master - WCAL_calibration[ipm].tmean)/WCAL_calibration[ipm].tsigma );
  }

  //V2 histo
//  int ipm=1;
//  if(e.V2.hasDigit) {
//    tdet->Fill(e.V2.t0ns());
//    //tdiffdet->Fill(e.V2.t0ns() - master);
//    tdiffdet->Fill( (e.V2.t0ns() - master - V2_calibration.tmean)/V2_calibration.tsigma );
//  }

  //T2 histo
//  int ipm=1;
//  if(e.T2.hasDigit) {
//    tdet->Fill(master);
//    //tdiffdet->Fill(master - master);
//    tdiffdet->Fill( (master - T2_calibration.tmean)/T2_calibration.tsigma );
//  }

  return;
}
