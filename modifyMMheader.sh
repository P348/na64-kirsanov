#!/bin/bash
#edit coordinates in ../mm.h
#manual fix of MM rotation to be compatible with 2021mu momentum reconstruction
sed -i.bak -r 's#\-Kx\*strip\_x \+ Ky\*strip\_y;#\+Kx\*strip\_x \- Ky\*strip\_y;#g' ../mm.h
