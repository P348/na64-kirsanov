  IPrint      = 1;
  Mode        = 1;
  ICalib      = 0;
  TimeCut     = 4.;
  MomentumLowerCut = -1.;
  MomentumUpperCut = 10000.;
  SRDLowerCut = 10.;
  SRDUpperCut = 200.;
  SRDEachLowerCut = 1.;
  WCAL0LowerCut = -1.;
  WCAL0UpperCut = 1000.;
  WCAL1LowerCut = -1.;
  WCAL1UpperCut = 1000.;
  WCATVetoUpperCut = 1000.;
  WCALSumLowerCut = 1.;
  WCALSumUpperCut = 130.;
  ECALLowerCut = 1.1;
  ECALUpperCut = 1000.;
  ECALLowerCut2 = 2.;
  PRSLowerCut = -1.;
  PRSUpperCut = 1000.;
  WCALECALLowerCut = 100.;
  WCALECALUpperCut = 220.;
  WCALECALLowerCut2 = 100.;
  ECALMaxCell = 3;
  ECALNarrowShower = -1;
  ECALChi2UpperCut = 100000.;
  
  WCAL2LowerCut = -1.;
  WCAL2UpperCut = 0.0008;
  V2LowerCut = -1.;
  V2UpperCut = 1000.;
  S4LowerCut = 0.0003;
  S4UpperCut = 1000.;

  VETOLowerCut = -1.;
  VETOUpperCut = 0.009;
  HCAL0LowerCut = -1.;
  HCAL0UpperCut = 1.;
  HCAL1LowerCut = -1.;
  HCAL1UpperCut = 1000.;
  HCAL2LowerCut = -1.;
  HCAL2UpperCut = 1000.;
  HCALSumLowerCut = -1.;
  HCALSumUpperCut = 1000.;
  ETotLowerCut = -1.;
